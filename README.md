Ola seja bem vindo ao meu repositório de estudos para Data Scientist.

Aqui estará meus estudos de Python referente ao curso Python do ZERO ao DS

Nesse curso, você vai desenvolver uma análise completa para um problema de negócio, começando do absoluto zero, com os fundamentos da linguagem Python e terminando com uma análise completa que pode ser acessada de qualquer dispositivo móvel. 

* Módulo 01 - Começando com Python <br/>
* Módulo 02 - Manipulação de Dados <br/>
* Módulo 03 - Estruturas de dados <br/>
* Módulo 04 - Estruturas de Controle <br/>
* Módulo 05 - Funções e Dashboards <br/>
* Módulo 06 - Dashboard com Streamlit <br/>
* Módulo 07 - Publicação do Dashboard no Heroku <br/>
* Módulo 08 - Projeto de Insights <br/>






