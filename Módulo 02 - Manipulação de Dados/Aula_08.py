#Aula 08 - Tipos de variáveis e conversões em Python
#função - read_csv
#Biblioteca - pandas
#essa biblioteca tem o apelido de pd

import pandas as pd
from numpy import int64

data = pd.read_csv('datasets/kc_house_data.csv')

#Funçao que converte de object ( string ) p/ date
data['date'] = pd.to_datetime(data['date'])

# Mostrar na tela os tipos de variaveis em cada coluna
print(data.dtypes)

#=======================================
# COMO converter os tipos de variaveis
#========================================



#Inteiro p/ float
data['bedrooms'] = data['bedrooms'].astype(float)

print(data.dtypes)
print(data[['id', 'bedrooms']].head(3))


#Float p/ inteiro
data['bedrooms'] = data['bedrooms'].astype(int64)
print(data.dtypes)

# inteiro p/ String
data['bedrooms'] = data['bedrooms'].astype(str)
print(data.dtypes)

# String  p/ inteiro
data['bedrooms'] = data['bedrooms'].astype(int64)
print(data.dtypes)

#String p/ Data
data['date'] = pd.to_datetime(data['date'])
print(data.dtypes)