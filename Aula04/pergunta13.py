## 13. Quantas casas tem vista para o mar?
# Estratégia:
# 1. Selecionar imóveis com a coluna 'waterfront' igual a 1.
# 2. Contar o número de imóveis nesse novo conjunto de dados.

houses = data.loc[data['floors'] == 1, 'id'].shape[0]

print( 'There are {} Houses with the number of floors grater than 2'.format( houses ) )

#There are 10680 Houses with the number of floors grater than 2
