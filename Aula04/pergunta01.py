## 1. Quantas casas estão disponíveis para compra?
# Eu vou considerar que a coluna "id" representa a identificação única do imóvel
num_houses_unique = data['id'].nunique()

# Resultado
print( 'Estão disponíveis {} imóveis'.format( num_houses_unique ) )


#Estão disponíveis 21436 imóveis

