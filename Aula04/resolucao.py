#Resolução
#Import Librarie
import numpy as np
import pandas as pd
import seaborn as sns
from matplotlib import pyplot as plt
import plotly.express as px

# Supress Scientific Notation
np.set_printoptions(suppress=True)
pd.set_option('display.float_format', '{:.2f}'.format)


#Loading Dat
# loading data into memory
data = pd.read_csv( '../kc_house_data.csv' )

