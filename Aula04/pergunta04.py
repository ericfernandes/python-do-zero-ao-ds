## 4. Qual a casa mais cara ( casa com o maior valor de venda )?

# Stratégia: Selecionar a coluna "id", "price", ordenar as casas pela coluna "price" em ordem decrescente e 
#   escolher o imóvel do primeiro id.
house_expensive = data[['id', 'price']].sort_values( 'price', ascending=False ).loc[0,'id']

print( 'Most expensive house: {}'.format( house_expensive ) )


#Most expensive house: 7129300520
