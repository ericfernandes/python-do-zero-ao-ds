## 10. Qual o preço mínimo entre as casas com 3 quartos?

# Estratégia:
# 1. Selecionar imóveis com 3 bathrooms.
# 2. Calcular o menor preço da coluna "price" do novo conjunto de dados

min_price = np.round( data.loc[data['bedrooms'] == 3, 'price'].min(), 2 )

print( 'Min Price: ${}'.format( min_price ) )

#Min Price: $82000.0
