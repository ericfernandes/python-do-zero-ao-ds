## 5. Qual a casa com o maior número de quartos?
# Estratégia:
# 1. Selecionar a coluna "id", "bedroom"
# 2. Ordenar os imóveis pelo numero de quartos de ordem decrescente
# 3. Selecionar a primeira coluna "id"

df = data[['id', 'bedrooms']].sort_values( 'bedrooms', ascending=False )

print( 'House with biggest number of bedrooms: {}'.format( df.iloc[0, 0] ) )

#House with biggest number of bedrooms: 2402100895
