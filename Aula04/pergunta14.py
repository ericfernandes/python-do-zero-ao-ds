## 14. Das casas com vista para o mar, quantas tem 3 quartos?
# Estratégia:
# 1. Selecionar imóveis com a coluna 'waterfront' igual a 1 e a coluna 'bedrooms' maior que 3.
# 2. Contar o número de imóveis nesse novo conjunto de dados.

houses = data.loc[(data['waterfront'] == 1) & (data['bedrooms'] > 2), 'id'].shape[0]

print( 'There are {} Houses with waterfront and bedrooms greater then 3'.format( houses ) )

#There are 127 Houses with waterfront and bedrooms greater then 3
