## 11. Quantas casas possuem mais de 300 metros quadrados na sala de estar?# Estratégia:
# 1. Selecionar imóveis com mais de 300 sqft_living.
# 2. Contar o número de imóveis nesse novo conjunto de dados.

houses = data.loc[data['sqft_living'] > 300, 'id'].shape[0]

print( 'There are {} Houses with living room grater than 300 square foot'.format( houses ) )
#There are 21612 Houses with living room grater than 300 square foot

