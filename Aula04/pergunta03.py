## 3. Quais são os atributos das casas?
# Strategia: Excluir as colunas "id", "date" e mostrar os atributos restantes
df = data.drop( ['id', 'date'], axis=1 )
print( df.columns.tolist() )

#['price', 'bedrooms', 'bathrooms', 'sqft_living', 'sqft_lot', 'floors', 'waterfront', 'view', 'condition', 'grade', 'sqft_above', 'sqft_basement', 'yr_built', 'yr_renovated', 'zipcode', 'lat', 'long', 'sqft_living15', 'sqft_lot15']
