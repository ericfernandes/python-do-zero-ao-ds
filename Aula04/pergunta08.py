## 8. Qual o preço médio de todas as casas no conjunto de dados?

# Estratégia:
# 1. Calcular o preço médio da coluna "price"
avg_price = np.round( data['price'].mean(), 2 )

print( 'Average Price: ${}'.format( avg_price ) )

#Average Price: $540088.14
