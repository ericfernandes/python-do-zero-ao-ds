## 9. Qual o preço médio de casas com 2 banheiros?
# Estratégia:
# 1. Selecionar imóveis com 2 banheiros.
# 2. Calcular o preço médio da coluna "price" do novo conjunto de dados

avg_price = np.round( data.loc[data['bathrooms'] == 2, 'price'].mean(), 2 )

print( 'Average Price: ${}'.format( avg_price ) )

#Average Price: $457889.72
