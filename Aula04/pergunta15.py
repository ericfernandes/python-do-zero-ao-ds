## 15. Das casas com mais de 300 metros quadrados de sala de estar, quantas tem mais de 2 banheiros?
# Estratégia:
# 1. Selecionar imóveis com a coluna 'sqft_living' maior que 300 e a coluna 'bathrooms' maior que 2.
# 2. Contar o número de imóveis nesse novo conjunto de dados.

houses = data.loc[(data['sqft_living'] > 300) & (data['bathrooms'] > 2), 'id'].shape[0]

print( 'There are {} Houses with living room greater than 300 and bathrooms bigger than 2'.format( houses ) )

#There are 11242 Houses with living room greater than 300 and bathrooms bigger than 2