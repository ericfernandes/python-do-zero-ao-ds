## 12. Quantas casas tem mais de 2 andares?
# Estratégia:
# 1. Selecionar imóveis com a coluna 'floors' maior que 2.
# 2. Contar o número de imóveis nesse novo conjunto de dados.

houses = data.loc[data['floors'] > 2, 'id'].shape[0]

print( 'There are {} Houses with the number of floors grater than 2'.format( houses ) )

#There are 782 Houses with the number of floors grater than 2
