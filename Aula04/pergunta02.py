## 2. Quantos atributos as casas possuem?


# O numero de colunas representam os atributos do apartamento.
# id e date - não são atributos do apartamento
num_attributes = len( data.columns ) - 2

# Resultado
print( 'Os imóveis posseum {} atributos'. format( num_attributes ) )


#Os imóveis posseum 19 atributos
