#função - read_csv
#Biblioteca - pandas
#essa biblioteca tem o apelido de pd

import pandas as pd
data = pd.read_csv('datasets/kc_house_data.csv')

# 5 primeiras linhas do conjunto de dados
print(data.head())

# numero de colunas e o numero de linhas do conjunto de dados
print(data.shape)

# nome das colunas do conjunto de dados
print(data.columns)

# mostre na tela o conjunto de dados ordenads pela coluna price
print(data[['id', 'price']].sort_values('price'))

# mostre na tela o conjunto de dados ordenados pela coluna price do maior ao menor
print(data[['id', 'price']].sort_values('price', ascending=False))