## 7. Quantas casas possuem 2 banheiros?

# Estratégia:
# 1. Filtrar linhas (imóveis) que possuem 2 banheiros.
# 2. Contar o número de linhas do dataset

df = data.loc[data['bathrooms'] == 2, :]
num_houses = len( df )

print( 'Total number of houses with 2 bathrooms: {}'.format( num_houses ) )

#Total number of houses with 2 bathrooms: 1930
